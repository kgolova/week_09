package ru.edu.task2.xml;

import org.junit.Test;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import static org.junit.Assert.assertTrue;

/**
 * ReadOnly
 */
@Configuration
@ComponentScan
public class AppXMLTest {

    @Test
    public void run() {
        assertTrue(AppXML.run().isValid());
    }
}